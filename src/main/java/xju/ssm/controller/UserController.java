package xju.ssm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import xju.ssm.entity.User;
import xju.ssm.service.UserService;

import java.util.List;


@Controller
public class UserController {
    //从IOC容器中找到匹配的实例注入进来.
    @Autowired
    UserService userService;

   /* @RequestMapping("/hello")
    public void test01(){
//        userService = new UserSeviceImpl();
        System.out.println("Hello World"+userService.queryAll());
    }*/

   @RequestMapping("/list")
    public ModelAndView queryAll(ModelAndView modelAndView){
       List<User> list = userService.queryAll();
       modelAndView.addObject("list",list);
       modelAndView.setViewName("success");
       return modelAndView;
   }

   @RequestMapping("/add")
    public String add(User user){
       int i=userService.add(user);
       return i!=0?"success":"fail";
   }

   @RequestMapping("/delete")
    public String delete(int id){
       int i = userService.delete(id);
       return i!=0?"success":"fail";
   }

   @RequestMapping("/update")
    public ModelAndView update(User user,ModelAndView modelAndView){
       int i = userService.update(user);
       ModelAndView mav = queryAll(modelAndView);

       return mav;
   }
}
