package xju.ssm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import xju.ssm.entity.User;

import java.util.List;

@Mapper
public interface UserMapper {

    @Update("insert into tb_user (name, password) values (#{name},#{password});")
    public int add(User user)throws Exception;

    @Select("select * from tb_user")
    public List<User> queryAll()throws Exception;

    @Update("delete tb_user where id = #{id}")
    public int delete(int id)throws Exception;

    @Update("update tb_user set name = #{name},password = #{password} where id=#{id}")
    public int update(User user) throws Exception;
}
