package xju.ssm.service;

import org.springframework.stereotype.Service;
import xju.ssm.entity.User;
import xju.ssm.mapper.UserMapper;

import java.util.List;

@Service
public interface UserService {

    public int add(User user);

    public List<User> queryAll();

    public int delete(int id);

    public int update(User user);
}
