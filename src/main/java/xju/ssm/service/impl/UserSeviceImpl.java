package xju.ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xju.ssm.entity.User;
import xju.ssm.mapper.UserMapper;
import xju.ssm.service.UserService;

import java.util.List;

@Service
public class UserSeviceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    //mybatis帮开发人员创建对应接口的实现类.

    @Override
    public int add(User user) {
        try {
            return userMapper.add(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<User> queryAll() {
        try {
            return userMapper.queryAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int delete(int id) {
        try {
            return userMapper.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(User user) {
        try {
            return userMapper.update(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


}
